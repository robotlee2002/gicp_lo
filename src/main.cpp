#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <string>
#include <Eigen/Dense>
#include <ctime>
#include <math.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/gicp.h>
#include <pcl/visualization/cloud_viewer.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Path.h>
#include "sensor_msgs/Imu.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

ros::Publisher g_pub;
pcl::PointCloud<pcl::PointXYZ>::Ptr g_cloud_pre;
Eigen::Matrix4d g_tm;
Eigen::Matrix4d g_tm_test;
ofstream g_file_in;
ifstream g_file_out;
volatile int g_frames_count = 0;
volatile bool first_msg_flag = true;
volatile bool g_dbg_print = false;
volatile float g_z_scale = 1.0;

const int ROWS = 25301;
const int COLS = 32;
double imu[ROWS][COLS] = {0};
volatile int pc2_msg_cnt = 0;
volatile int dbg_cnt = 0;

using namespace std;

void print4x4Matrix(const Eigen::Matrix4d & m)
{
  // printf("Transformation matrix :\n");
  printf("    | %6.3f %6.3f %6.3f %6.3f | \n", m(0, 0), m(0, 1), m(0, 2), m(0, 3));
  printf("T = | %6.3f %6.3f %6.3f %6.3f | \n", m(1, 0), m(1, 1), m(1, 2), m(1, 3));
  printf("    | %6.3f %6.3f %6.3f %6.3f | \n", m(2, 0), m(2, 1), m(2, 2), m(2, 3));
  printf("    | %6.3f %6.3f %6.3f %6.3f | \n", m(3, 0), m(3, 1), m(3, 2), m(3, 3));
}

void print4x4Matrix_RT(const Eigen::Matrix4d & m)
{
	printf("Rotation m :\n");
	printf("    | %6.3f %6.3f %6.3f | \n", m(0, 0), m(0, 1), m(0, 2));
	printf("R = | %6.3f %6.3f %6.3f | \n", m(1, 0), m(1, 1), m(1, 2));
	printf("    | %6.3f %6.3f %6.3f | \n", m(2, 0), m(2, 1), m(2, 2));
	printf("Translation vector :\n");
	printf("t = < %6.3f, %6.3f, %6.3f >\n\n", m(0, 3), m(1, 3), m(2, 3));
}

class PubClass
{
	public:
		PubClass() { };
		ros::Publisher PosePub;
		ros::Publisher PathPub;
		nav_msgs::Path pathMsg;
};
PubClass *g_pub_ptr;

void publish_pose_path(tf::Transform & t, double pc_ts) {
  // Publish Pose
  geometry_msgs::PoseStamped pose_msg_;

  std::string fixed_frame_ = "world";
  pose_msg_.header.frame_id = fixed_frame_;

  ros::Time my_time;
  my_time.fromSec(pc_ts);

  pose_msg_.header.stamp = my_time;

  tf::poseTFToMsg(t, pose_msg_.pose);

  g_pub_ptr->PosePub.publish(pose_msg_);

  // Publish Path
  g_pub_ptr->pathMsg.header.frame_id = fixed_frame_;
  g_pub_ptr->pathMsg.header.stamp = my_time;

  g_pub_ptr->pathMsg.poses.push_back(pose_msg_);
  g_pub_ptr->PathPub.publish(g_pub_ptr->pathMsg);
}

Eigen::Quaterniond euler2Quaternion(const double roll, const double pitch, const double yaw)
{
//	Eigen::AngleAxisd rollAngle((roll*M_PI) / 180, Eigen::Vector3d::UnitX());
//	Eigen::AngleAxisd pitchAngle((pitch*M_PI) / 180, Eigen::Vector3d::UnitY());
//	Eigen::AngleAxisd yawAngle((yaw*M_PI) / 180, Eigen::Vector3d::UnitZ());
	Eigen::AngleAxisd rollAngle(roll, Eigen::Vector3d::UnitX());
	Eigen::AngleAxisd pitchAngle(pitch, Eigen::Vector3d::UnitY());
	Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitZ());

	Eigen::Quaterniond q = rollAngle * yawAngle * pitchAngle;
	return q;
}

bool first_flag_publish_tf = true;
tf::Transform g_t1;

void publish_tf(const Eigen::Matrix4d & m, double pc_ts) {
	//	cv::Mat Rwc = m.rowRange(0,3).colRange(0,3).t();
	//	cv::Mat twc = -Rwc*m.rowRange(0,3).col(3);
	//	tf::Matrix3x3 M(Rwc.at<float>(0,0),Rwc.at<float>(0,1),Rwc.at<float>(0,2),
	//		Rwc.at<float>(1,0),Rwc.at<float>(1,1),Rwc.at<float>(1,2),
	//		Rwc.at<float>(2,0),Rwc.at<float>(2,1),Rwc.at<float>(2,2));
	//	tf::Vector3 V(twc.at<float>(0), twc.at<float>(1), twc.at<float>(2));

	//	tf::Matrix3x3 M(m(0, 0), m(0, 1), m(0, 2),
	//					m(1, 0), m(1, 1), m(1, 2),
	//					m(2, 0), m(2, 1), m(2, 2));
	//	tf::Vector3 V(m(0, 3), m(1, 3), m(2, 3));
	//	tf::Transform tfTcw(M, V);

	//	Eigen::Transform<float, 3, Eigen::Affine> tROTA(m);
	//	float x, y, z, roll, pitch, yaw;
	//	pcl::getTranslationAndEulerAngles(m, &x, &y, &z, &roll, &pitch, &yaw);

	double x, y, z, roll, pitch, yaw;
	x = m(0, 3);
	y = m(1, 3);
	z = m(2, 3);
	z /= g_z_scale;
	roll = atan2(m(2, 1), m(2, 2));
	pitch = asin(-m(2, 0));
	yaw = atan2(m(1, 0), m(0, 0));
	printf("x, y, z, roll, pitch, yaw = %6.3f, %6.3f, %6.3f, %6.3f, %6.3f, %6.3f\n",
				x, y, z, roll, pitch, yaw);

//	// Write pose into file
//	if (2 == g_frames_count) {
//	  g_file_in.open("/home/df/data/Velodyne_parking_garage/gicp_lo.txt", std::fstream::trunc);
//	} else {
//	  g_file_in.open("/home/df/data/Velodyne_parking_garage/gicp_lo.txt", std::fstream::app);
//	}
//	char pose[100];
//	sprintf(pose, "%6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f\n", x, y, z, roll, pitch, yaw);
//	g_file_in << pose;
//	g_file_in.close();

	// 4x4 Transformation Matrix to tf Transform
	Eigen::Matrix4d Tm = m;
	tf::Vector3 origin;
	origin.setValue(static_cast<double>(Tm(0,3)),static_cast<double>(Tm(1,3)),static_cast<double>(Tm(2,3)));
	tf::Matrix3x3 tf3d;
	tf3d.setValue(static_cast<double>(Tm(0,0)), static_cast<double>(Tm(0,1)), static_cast<double>(Tm(0,2)),
		static_cast<double>(Tm(1,0)), static_cast<double>(Tm(1,1)), static_cast<double>(Tm(1,2)),
		static_cast<double>(Tm(2,0)), static_cast<double>(Tm(2,1)), static_cast<double>(Tm(2,2)));
	tf::Quaternion tfqt;
	tf3d.getRotation(tfqt);
	tf::Transform t;
	t.setOrigin(origin);
	t.setRotation(tfqt);
	cout << "publish_tf = " << endl;
	cout << m << endl;

	// TODO adjust t
	// 4x4 Transformation Matrix to tf Transform: use self calculate
	tf::Transform t2;
	t2.setOrigin(tf::Vector3(0, 0, 0));
	tf::Quaternion q2;
	q2.setRPY(0, 0, 90 * M_PI/180);
	t2.setRotation(q2);
	t =  t2 * t;
	tf::Transform t3;
	t3.setOrigin(tf::Vector3(0, 0, 0));
	tf::Quaternion q3;
	q3.setRPY(-90 * M_PI/180, 0, 0);
	t3.setRotation(q3);
	t =  t3 * t;

	// adjust local oritation
	tf::Transform t31;
	t31.setOrigin(tf::Vector3(0, 0, 0));
	tf::Quaternion q31;
	q31.setRPY(90 * M_PI/180, 0, 0);
	t31.setRotation(q31);
	t =  t * t31;
	tf::Transform t32;
	t32.setOrigin(tf::Vector3(0, 0, 0));
	tf::Quaternion q32;
	q32.setRPY(0, -90 * M_PI/180, 0);
	t32.setRotation(q32);
	t =  t * t32;

	ros::Time my_time;
	my_time.fromSec(pc_ts);
	cout << "publish_tf: time = " << my_time << endl;

	// bing
	tf::Vector3 my_t = t.getOrigin();
	tf::Quaternion my_q = t.getRotation();
	tf::Matrix3x3 my_mat(my_q);
	double rr, pp, yy;
	my_mat.getRPY(rr, pp, yy);
	rr = rr * 180 / M_PI;
	pp = pp * 180 / M_PI;
	yy = yy * 180 / M_PI;

//	printf("x, y, z, roll, pitch, yaw == %6.3f, %6.3f, %6.3f, %6.3f, %6.3f, %6.3f\n",
//			my_t[0], my_t[1], my_t[2], rr, pp, yy);

	// Write pose into file
	if (true == first_flag_publish_tf) {
	  g_file_in.open("/home/df/data/Berkeley_parking_garage/gicp_lo.csv", std::fstream::trunc);
	  first_flag_publish_tf = false;
	} else {
	  g_file_in.open("/home/df/data/Berkeley_parking_garage/gicp_lo.csv", std::fstream::app);
	}
	char pose_str[1000];
	double WEEK_S = 1911 * 7 * 24 * 60 * 60;
	double time_lo = pc_ts - WEEK_S;
	sprintf(pose_str, "%.8f, %.8f, %.8f, %.8f, %.8f, %.8f, %.8f, %.8f, %.8f, %.8f, %.8f\n",
			time_lo, my_t[0], my_t[1], my_t[2], rr, pp, yy, my_q[0], my_q[1], my_q[2], my_q[3]);
	g_file_in << pose_str;
	g_file_in.close();
	// cout << pose_str << endl;

	static tf::TransformBroadcaster br;
	br.sendTransform(tf::StampedTransform(t, my_time, "/world", "/lo"));

	publish_pose_path(t, pc_ts);
}

void publish_tf_backup(const Eigen::Matrix4d & m) {
  tf::Vector3 origin;
  origin.setValue(static_cast<double>(m(0,3)),static_cast<double>(m(1,3)),static_cast<double>(m(2,3)));

  cout << origin << endl;
  tf::Matrix3x3 tf3d;
  tf3d.setValue(static_cast<double>(m(0,0)), static_cast<double>(m(0,1)), static_cast<double>(m(0,2)),
  static_cast<double>(m(1,0)), static_cast<double>(m(1,1)), static_cast<double>(m(1,2)),
  static_cast<double>(m(2,0)), static_cast<double>(m(2,1)), static_cast<double>(m(2,2)));

  tf::Quaternion tfqt;
  tf3d.getRotation(tfqt);

  tf::Transform t;
  t.setOrigin(origin);
  t.setRotation(tfqt);

  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(t, ros::Time::now(), "world", "lo"));

//  static tf::TransformBroadcaster br2;
//  tf::Transform transform2;
//  br2.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "world", "velodyne"));

  // publish_pose_path(t);

  return;
}

int find_closest_time_id(double ts) {
	double diff_min = 10000000;
	int id = 0;
	for (int i = 0; i < ROWS; i++) {
		double t = imu[i][1];
		double diff = fabs(t - ts);
		if (diff < diff_min) {
			id = i;
			diff_min = diff;
		}
	}

	return id;
}

//int ROWS = 25501;
//int COLS = 32;
//double imu[25501][32] = {0};
Eigen::Matrix4d g_m_init_prev;
bool g_with_imu = true;

void cloud_proc(const sensor_msgs::PointCloud2ConstPtr& pc2_msg)
{
	g_frames_count += 1;
	pc2_msg_cnt += 1;
	printf("\n\ng_frames_count = %d, pc2_msg_cnt = %d\n", g_frames_count, pc2_msg_cnt);

	int s  = pc2_msg->header.stamp.sec;
	int ns = pc2_msg->header.stamp.nsec;
	double pc_ts = s + ns/1e9;

	double tmp = 1e9;
	int WEEK_S = 1911 * 7 * 24 * 60 * 60;
	s = s - WEEK_S;
	double ts = s + ns / tmp; // ts is only sec, without weeks
	int id = 0;

  Eigen::Matrix4d m_init(4, 4);
  if (true == g_with_imu) {

	  id = find_closest_time_id(ts);

	  for (int i = 0; i < 3; i++)
		  for (int j = 0; j < 3; j++) {
			  m_init(i, j) = imu[id][9 + i * 3 + j];
		  }
	  for (int i = 0; i < 3; i++)
		  m_init(i, 3) = imu[id][3 + i];
	  for (int i = 0; i < 3; i++)
		  m_init(3, i) = 0;
	  m_init(3, 3) = 1;
  }

/*
//	  cout << pc2_msg->header << endl;
//	  printf("timestamp sec  = %.4f\n", (float) pc2_msg->header.stamp.sec); // ERROR !
//	  printf("timestamp nsec = %.4f\n", (float) pc2_msg->header.stamp.nsec);
//
//	  int s  = pc2_msg->header.stamp.sec;
//	  int ns = pc2_msg->header.stamp.nsec;
//	  printf("timestamp sec  = %d\n", s);
//	  printf("timestamp nsec = %d\n", ns);
//	  double tmp = 1e9;
//	  double ts = s + ns / tmp;
//	  printf("ts = %.9f\n", ts);

//	  ros::Time begin = ros::Time::now();
//	  printf("time_now sec   = %.8f\n", (float)begin.sec);
//	  printf("time_now nsec  = %.8f\n", (float)begin.nsec);

//	  if (msg_cnt <= 1) {
//		  for (int i = 0; i < 1; i++)
//			  for (int j = 0; j < COLS; j++) {
//				  printf("  %.4f\n", imu[i][j]);
//			  }
//		  printf("............");
//		  for (int i = (ROWS - 1); i < ROWS; i++)
//			  for (int j = 0; j < COLS; j++) {
//				  printf("  %.4f\n", imu[i][j]);
//			  }
//	  }
*/

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZ>);

  int width  = pc2_msg->width;
  int height = pc2_msg->height;
  int point_step = pc2_msg->point_step;
  int row_step = pc2_msg->row_step;
  bool is_dense = pc2_msg->is_dense;

  // printf("\nwidth, height = %d, %d\n", width, height);
  // printf("point_step, row_step = %d, %d\n", point_step, row_step);
  // printf("is_dense = %B\n", is_dense);

  if(true == first_msg_flag) {
	  // printf("\n");

	  g_cloud_pre = cloud_in;
	  pcl::fromROSMsg(*pc2_msg, *g_cloud_pre);
//	  int size = g_cloud_pre->points.size();
//	  for (int i = 0; i < size; i++) {
//		g_cloud_pre->points[i].data[2] *= g_z_scale;
//	  }
	  if (true == g_with_imu) {
		  cout << pc2_msg->header.stamp << endl;
		  printf("PointCloud2 message: pc_ts = %.9f\n", pc_ts);
		  printf("imu[%d][0] = %.6f\n", id, imu[id][0]);
		  printf("imu[%d][1] = %.6f\n", id, imu[id][1]);
		  printf("imu[%d][2] = %.6f\n", id, imu[id][2]);
		  cout << m_init << endl;

		  printf("imu[id][:] =\n");
		  for (int j = 0; j < COLS; j++) {
			  printf("  %.4f\n", imu[id][j]);
		  }

		  g_m_init_prev = m_init.replicate(1, 1);
		  Eigen::Matrix4d m_init_first  = m_init.replicate(1, 1);
		  g_tm = m_init_first *  g_tm;
		  g_tm_test = m_init_first *  g_tm_test;
		  publish_tf(g_tm_test, pc_ts);
	  }
	  first_msg_flag = false;
	  return;
  } else {
	  pcl::fromROSMsg(*pc2_msg, *cloud_in);
  }

//  double x_min, y_min, z_min;
//  double x_max, y_max, z_max;
//  x_min =  1000000;
//  y_min =  1000000;
//  z_min =  1000000;
//  x_max = -1000000;
//  y_max = -1000000;
//  z_max = -1000000;
//
//  // not use, try to scale z for better ICP since x, y are big values, but z
//  // set all the point.data[3] values to 1 to aid the rigid transformation
//  double x, y, z;
//  int size = cloud_in->points.size();
//  for (int i = 0; i < size; i++) {
//	  x = cloud_in->points[i].data[0];
//	  y = cloud_in->points[i].data[1];
//	  z = cloud_in->points[i].data[2];
//	  if (x < x_min) {
//		  x_min = x;
//	  }
//	  if (x > x_max) {
//		  x_max = x;
//	  }
//	  if (y < y_min) {
//		  y_min = y;
//	  }
//	  if (y > y_max) {
//		  y_max = y;
//	  }
//	  if (z < z_min) {
//		  z_min = z;
//	  }
//	  if (z > z_max) {
//		  z_max = z;
//	  }
//
//	  cloud_in->points[i].data[2] *= g_z_scale;
//  }

  clock_t begin = clock();

  Eigen::Matrix4d H;
  Eigen::Matrix4d g_tm_use;
  if (true == g_with_imu) {

	  Eigen::Matrix4d reverse = g_m_init_prev.inverse();
	  H = m_init * reverse;

//	  // m_init = H g_m_init_prev
//	  // thus: H = m_init g_m_init_prev^-1
//	  cout << "g_m_init_prev" << endl;
//	  cout << g_m_init_prev << endl;
//
//	  cout << "m_init" << endl;
//	  cout << m_init << endl;
//
//	  cout << "H" << endl;
//	  cout << H << endl;
//
//	  cout << "H * g_m_init_prev" << endl;
//	  cout << H * g_m_init_prev << endl;

	  g_m_init_prev  = m_init.replicate(1, 1);
  }

  // Defining a rotation matrix and translation vector
  // Eigen::Matrix4d m_init = Eigen::Matrix4d::Identity(); // init from IMU prediction
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tr(new pcl::PointCloud<pcl::PointXYZ>);
  cloud_tr->resize(cloud_in->size());
  if (true == g_with_imu) {
	  pcl::transformPointCloud(*g_cloud_pre, *cloud_tr, H);
	  clock_t end1 = clock();
	  double elapsed_secs1 = double(end1 - begin) / CLOCKS_PER_SEC;
	  printf("elapsed_secs for tf = %.4f\n", elapsed_secs1);
  }

  // pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

//  icp.setMaxCorrespondenceDistance(6); // xy_fine, z_error
//  icp.setTransformationEpsilon(1e-14);
//  icp.setEuclideanFitnessEpsilon(0.001);

  // Set the maximum distance between two correspondences(src<->tgt), unit: m
  // Note: adjust this based on the size of your datasets
  icp.setMaxCorrespondenceDistance(1);   //  4  6
  icp.setTransformationEpsilon(1e-10);   // 14 12
  icp.setEuclideanFitnessEpsilon(0.3);  //  5  4
  icp.setMaximumIterations(100);

//  icp.setRANSACIterations(100);
//  icp.setRANSACOutlierRejectionThreshold(1);

  double dis = icp.getMaxCorrespondenceDistance(); // default value 5.0
  int iter = icp.getMaximumIterations(); // default value 200

  icp.setInputSource(cloud_in); // new
  icp.setInputTarget(g_cloud_pre); // old # g_cloud_pre cloud_tr
  pcl::PointCloud<pcl::PointXYZ> Final;
  icp.align(Final);

  g_cloud_pre = cloud_in;

//  // Convert the pcl/PointCloud to sensor_msgs/PointCloud2
//  sensor_msgs::PointCloud2 output; // Create a container
//  pcl::toROSMsg(*cloud_in, output);
//  g_pub.publish(output);

  if(icp.hasConverged()) {
	  double score = icp.getFitnessScore();
	  clock_t end = clock();
	  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	  // if (true == g_dbg_print || 2 == g_frames_count % 10 || score > 4)
	  {
		printf("Count = %d, Time cost %.4f, Max iter = %d, Max dis = %.2f, G-ICP converged score %.4f\n", \
				g_frames_count, elapsed_secs, iter, dis, score);
		//		printf("point number = %d\n", size);
		//		printf("x(%6.6f ~ %6.6f)\n", x_min, x_max);
		//		printf("y(%6.6f ~ %6.6f)\n", y_min, y_max);
		//		printf("z(%6.6f ~ %6.6f)\n", z_min, z_max);
	  }

    // std::cout << "\nICP transformation " << iterations << " : cloud_icp -> cloud_in" << std::endl;
	Eigen::Matrix4d m = icp.getFinalTransformation().cast<double>();
	// bing
    // g_tm = m * m_init * g_tm; // not work, TODO: why not this
	//	g_tm = m * H * g_tm;
	//	// g_tm = g_tm * g_m_init_first;

	// g_tm = g_tm * m; // from the visualization, it's correct

	cout << "g_tm = " << endl;
	cout << g_tm << endl;

	cout << "m = " << endl;
	cout << m << endl;
	//
	//	cout << "H" << endl;
	//	cout << H << endl;
	//
	//	cout << " m * H" << endl;
	//	cout <<  m * H << endl;

	g_tm = g_tm * m; // works

	g_tm_test =  H * g_tm_test;

	//    if (true == g_dbg_print || 2 == g_frames_count % 30) {
	//    	print4x4Matrix(m * m_init);
	//    	print4x4Matrix(g_tm);
	//    }
  } else {
    PCL_ERROR("\nICP has not converged !!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    return;
  }

  publish_tf(g_tm, pc_ts); // g_tm, g_tm_test
}

void imu_proc(const sensor_msgs::Imu::ConstPtr& msg) {
  ROS_INFO("Imu Seq: [%d]", msg->header.seq);
  ROS_INFO("Imu Orientation x: [%f], y: [%f], z: [%f], w: [%f]",
		  msg->orientation.x,msg->orientation.y,msg->orientation.z,msg->orientation.w);
}

int main(int argc, char** argv) {
  // Initialize ROS
  ros::init(argc, argv, "pcl_gicp");
  ros::NodeHandle nh;
  ros::Rate r(50);

  ifstream file( "/home/df/data/Berkeley_parking_garage/HT000_1471897849/201-246_plus1s_pose.csv" );
  std::string line;
  int col = 0;
  int row = 0;
  int col_tmp = 0;
  int row_tmp = 0;
  while( std::getline( file, line ) )
  {
    std::istringstream iss( line );
    std::string result;
    while( std::getline( iss, result, ',' ) )
    {
      imu[row][col] = std::atof( result.c_str() );
      col = col+1;
      col_tmp = col;
      row_tmp = row;
    }
    row = row+1;
    col = 0;
  }

//  ROS_INFO("row_tmp = [%d]\n", row_tmp);
//  ROS_INFO("col_tmp = [%d]\n", col_tmp);
//  printf("row_tmp = [%d]\n", row_tmp);
//  printf("col_tmp = [%d]\n", col_tmp);
//  return 0;

  g_tm = Eigen::Matrix4d::Identity();
  g_tm_test = Eigen::Matrix4d::Identity();

  PubClass myPub;
  g_pub_ptr = &myPub;
  g_pub_ptr->PosePub = nh.advertise<geometry_msgs::PoseStamped>("/lo/pose", 1000);
  g_pub_ptr->PathPub = nh.advertise<nav_msgs::Path>("/lo/path", 1000);

  // Create a ROS subscriber for the "input" point cloud
  // ros::Subscriber sub = nh.subscribe("input", 1, cloud_proc);
  ros::Subscriber sub = nh.subscribe("/velodyne_points", 10, cloud_proc);

  ros::Subscriber sub_imu = nh.subscribe("/imu_data", 100, imu_proc);

  // Create a ROS publisher for the output point cloud
  g_pub = nh.advertise<sensor_msgs::PointCloud2>("output", 1);

  // Spin
  ros::spin();
  g_file_in.close();
}
